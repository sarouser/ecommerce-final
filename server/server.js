const express = require("express")
const mongoose = require("mongoose")
const bodyParser = require("body-parser")
const morgan = require("morgan")
const cors = require("cors")
require("dotenv").config()

//app
const app = express();

//db
const db_config = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: true
}
mongoose.connect(process.env.DATABASE, db_config)
    .then(() => console.log("DB connected"))
    .catch((err) => console.log("DB connection error -> ", err));

//middlewares
app.use(morgan("dev"))
app.use(bodyParser.json({limit: "2mb"}));
app.use(cors());

//route
app.get('/api', ((req, res) => {
    res.json({
        data: "hey, node API"
    });
}));

//port
const port = process.env.PORT;

app.listen(port,()=> console.log(`Listening to port:${port}`));